# output (no trace)

```shell
emptyTree.collect(): []
emptyTree.breadthArrays(): []
emptyTree.toString(): /

/.equals(empty): false
/.equals(tree): false
/.countNodes(): 0
/.countLeaves(): 0
/.isEmpty(): true

tree.collect(): [1, 2, 4, 5, 3]
tree.breadthArrays(): [[1], [2, 3], [4, 5]]
tree.toString(): 1

1.equals(empty): false
1.equals(tree): true
1.countNodes(): 5
1.countLeaves(): 3
1.isEmpty(): false

1.left(): 2
1.right(): 3
1.leftMost(): 4
1.rightMost(): 3
1.left().right(): 5
1.right().left(): /

/.copy().breadthArrays(): []
/.mirror().breadthArrays(): []
1.copy().breadthArrays(): [[1], [2, 3], [4, 5]]
1.mirror().breadthArrays: [[1], [3, 2], [5, 4]]
1.copy().collect(): [1, 2, 4, 5, 3]
1.mirror().collect(): [1, 3, 2, 5, 4]

/.contains(/): false
1.contains(/): true
1.contains("0"): false
1.contains(1): true
1.contains("2"): true
1.contains(3): true

/.searchLeftRight("/"): /
/.searchLeftRight("0"): /
/.searchLeftRight("1"): /
1.searchLeftRight("/"): /
1.searchLeftRight("0"): /
1.searchLeftRight("1"): 1
1.searchLeftRight("2"): 2
1.searchLeftRight("3"): 3

tree = tree.insertRight("7")
1.breadthArrays(): [[1], [2, 3], [4, 5, 7]]
tree.describePathTo("7"): [right, right]

tree = tree.insertLeft("8")
1.breadthArrays(): [[1], [2, 3], [4, 5, 7], [8]]
tree.describePathTo("8"): [left, left, left]

tree = tree.insert("9")    # tries left, then right, then goes left
1.breadthArrays(): [[1], [2, 3], [4, 5, 7], [8, 9]]
tree.describePathTo("9"): [left, left, right]

tree = tree.insertBreadthFirst("6")
1.breadthArrays(): [[1], [2, 3], [4, 5, 6, 7], [8, 9]]
tree.describePathTo("6"): [right, left]

tree = tree.insertBreadthFirst("10")
1.breadthArrays(): [[1], [2, 3], [4, 5, 6, 7], [8, 9, 10]]
tree.describePathTo("10"): [left, right, left]

tree = tree.insertBreadthFirst("6")    # duplicate on purpose
1.breadthArrays(): [[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6]]
tree.describePathTo("6"): [right, left]    # gives shortest path

/.pathTo(/): [/]
/.pathTo(1): []
1.pathTo(/): [1, 3, 6, /]    # shortest path to free space
1.pathTo(0): []
1.pathTo(1): [1]
1.shortestPathTo("10"): [1, 2, 5, 10]
1.pathTo(6): [1, 2, 5, 6]    # duplicate "6" selected manually
1.shortestPathTo("6"): [1, 3, 6]

tree = tree.insertRightOf(6, 13)
[[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6, 13]]
tree.describePathTo("13"): [right, left, right]

tree = tree.insertLeftOf(7, 14)
[[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6, 13, 14]]
tree.describePathTo("14"): [right, right, left]

```



# output (with trace)

```shell
emptyTree.collect(): []
emptyTree.breadthArrays(): []
emptyTree.toString(): /

/.equals(empty): false
/.equals(tree): false
/.countNodes(): 0
/.countLeaves(): 0
/.isEmpty(): true

tree.collect(): [1, 2, 4, 5, 3]
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5]], 1)
tree.breadthArrays(): [[1], [2, 3], [4, 5]]
tree.toString(): 1

1.equals(empty): false
1.equals(tree): true
1.countNodes(): 5
1.countLeaves(): 3
1.isEmpty(): false

1.left(): 2
1.right(): 3
1.leftMost(): 4
1.rightMost(): 3
1.left().right(): 5
1.right().left(): /

/.copy().breadthArrays(): []
/.mirror().breadthArrays(): []
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5]], 1)
1.copy().breadthArrays(): [[1], [2, 3], [4, 5]]
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [3]], 1)
# LiveTree.breadthArrays([[1], [3, 2], []], 2)
# LiveTree.breadthArrays([[1], [3, 2], [5]], 2)
1.mirror().breadthArrays: [[1], [3, 2], [5, 4]]
1.copy().collect(): [1, 2, 4, 5, 3]
1.mirror().collect(): [1, 3, 2, 5, 4]

/.contains(/): false
1.contains(/): true
1.contains("0"): false
1.contains(1): true
1.contains("2"): true
1.contains(3): true

/.searchLeftRight("/"): /
/.searchLeftRight("0"): /
/.searchLeftRight("1"): /
1.searchLeftRight("/"): /

# LiveTree.searchLeftRight("0")
# EmptyTree.searchLeftRight("0", [  ])
# EmptyTree.searchLeftRight("0", [ 3 ])
# EmptyTree.searchLeftRight("0", [ 3, 5 ])
# EmptyTree.searchLeftRight("0", [ 3, 5, / ])
# EmptyTree.searchLeftRight("0", [ 5, / ])
# EmptyTree.searchLeftRight("0", [ 5, /, / ])
# EmptyTree.searchLeftRight("0", [ /, / ])
# EmptyTree.searchLeftRight("0", [ /, /, / ])
# EmptyTree.searchLeftRight("0", [ /, / ])
# EmptyTree.searchLeftRight("0", [ / ])
# EmptyTree.searchLeftRight("0", [  ])
1.searchLeftRight("0"): /

# LiveTree.searchLeftRight("1")
# EmptyTree.searchLeftRight("1", [  ])
1.searchLeftRight("1"): 1

# LiveTree.searchLeftRight("2")
# EmptyTree.searchLeftRight("2", [  ])
# EmptyTree.searchLeftRight("2", [ 3 ])
1.searchLeftRight("2"): 2

# LiveTree.searchLeftRight("3")
# EmptyTree.searchLeftRight("3", [  ])
# EmptyTree.searchLeftRight("3", [ 3 ])
# EmptyTree.searchLeftRight("3", [ 3, 5 ])
# EmptyTree.searchLeftRight("3", [ 3, 5, / ])
# EmptyTree.searchLeftRight("3", [ 5, / ])
1.searchLeftRight("3"): 3

tree = tree.insertRight("7")
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5]], 2)
1.breadthArrays(): [[1], [2, 3], [4, 5, 7]]
# LiveTree.shortestPath("7")
# LiveTree.shortestPath("7", [], [])
# LiveTree.shortestPath("7", [1], [])
# LiveTree.shortestPath("7", [1, 2], [])
# LiveTree.shortestPath("7", [1, 2], [])
# LiveTree.shortestPath("7", [1], [])
# LiveTree.shortestPath("7", [1, 3], [])
# LiveTree.shortestPath: found key. Path = [1, 3, 7]
tree.describePathTo("7"): [right, right]

tree = tree.insertLeft("8")
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8]], 2)
1.breadthArrays(): [[1], [2, 3], [4, 5, 7], [8]]
# LiveTree.shortestPath("8")
# LiveTree.shortestPath("8", [], [])
# LiveTree.shortestPath("8", [1], [])
# LiveTree.shortestPath("8", [1, 2], [])
# LiveTree.shortestPath("8", [1, 2, 4], [])
# LiveTree.shortestPath: found key. Path = [1, 2, 4, 8]
# LiveTree.shortestPath("8", [1, 2], [1, 2, 4, 8])
# LiveTree.shortestPath("8", [1], [1, 2, 4, 8])
# LiveTree.shortestPath("8", [1, 3], [1, 2, 4, 8])
tree.describePathTo("8"): [left, left, left]

tree = tree.insert("9")    # tries left, then right, then goes left
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9]], 2)
1.breadthArrays(): [[1], [2, 3], [4, 5, 7], [8, 9]]
# LiveTree.shortestPath("9")
# LiveTree.shortestPath("9", [], [])
# LiveTree.shortestPath("9", [1], [])
# LiveTree.shortestPath("9", [1, 2], [])
# LiveTree.shortestPath("9", [1, 2, 4], [])
# LiveTree.shortestPath("9", [1, 2, 4], [])
# LiveTree.shortestPath: found key. Path = [1, 2, 4, 9]
# LiveTree.shortestPath("9", [1, 2], [1, 2, 4, 9])
# LiveTree.shortestPath("9", [1], [1, 2, 4, 9])
# LiveTree.shortestPath("9", [1, 3], [1, 2, 4, 9])
tree.describePathTo("9"): [left, left, right]

tree = tree.insertBreadthFirst("6")
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9]], 2)
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9]], 2)
1.breadthArrays(): [[1], [2, 3], [4, 5, 6, 7], [8, 9]]
# LiveTree.shortestPath("6")
# LiveTree.shortestPath("6", [], [])
# LiveTree.shortestPath("6", [1], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1], [])
# LiveTree.shortestPath("6", [1, 3], [])
# LiveTree.shortestPath: found key. Path = [1, 3, 6]
# LiveTree.shortestPath("6", [1, 3], [1, 3, 6])
tree.describePathTo("6"): [right, left]

tree = tree.insertBreadthFirst("10")
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9]], 2)
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9, 10]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10]], 2)
1.breadthArrays(): [[1], [2, 3], [4, 5, 6, 7], [8, 9, 10]]
# LiveTree.shortestPath("10")
# LiveTree.shortestPath("10", [], [])
# LiveTree.shortestPath("10", [1], [])
# LiveTree.shortestPath("10", [1, 2], [])
# LiveTree.shortestPath("10", [1, 2, 4], [])
# LiveTree.shortestPath("10", [1, 2, 4], [])
# LiveTree.shortestPath("10", [1, 2], [])
# LiveTree.shortestPath("10", [1, 2, 5], [])
# LiveTree.shortestPath: found key. Path = [1, 2, 5, 10]
# LiveTree.shortestPath("10", [1], [1, 2, 5, 10])
# LiveTree.shortestPath("10", [1, 3], [1, 2, 5, 10])
# LiveTree.shortestPath("10", [1, 3], [1, 2, 5, 10])
tree.describePathTo("10"): [left, right, left]

tree = tree.insertBreadthFirst("6")    # duplicate on purpose
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9, 10]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10]], 2)
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10, 6]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9, 10, 6]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10, 6]], 2)
1.breadthArrays(): [[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6]]
# LiveTree.shortestPath("6")
# LiveTree.shortestPath("6", [], [])
# LiveTree.shortestPath("6", [1], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 5], [])
# LiveTree.shortestPath("6", [1, 2, 5], [])
# LiveTree.shortestPath: found key. Path = [1, 2, 5, 6]
# LiveTree.shortestPath("6", [1], [1, 2, 5, 6])
# LiveTree.shortestPath("6", [1, 3], [1, 2, 5, 6])
# LiveTree.shortestPath: found key. Path = [1, 3, 6]
# LiveTree.shortestPath("6", [1, 3], [1, 3, 6])
tree.describePathTo("6"): [right, left]    # gives shortest path

/.pathTo(/): [/]

/.pathTo(1): []

# LiveTree.shortestPath("/")
# LiveTree.shortestPath("/", [], [])
# LiveTree.shortestPath("/", [1], [])
# LiveTree.shortestPath("/", [1, 2], [])
# LiveTree.shortestPath("/", [1, 2, 4], [])
# EmptyTree.shortestPath: found shorter path: [1, 2, 4, 8, /]
# LiveTree.shortestPath("/", [1, 2, 4], [1, 2, 4, 8, /])
# LiveTree.shortestPath("/", [1, 2], [1, 2, 4, 8, /])
# LiveTree.shortestPath("/", [1, 2, 5], [1, 2, 4, 8, /])
# LiveTree.shortestPath("/", [1, 2, 5], [1, 2, 4, 8, /])
# LiveTree.shortestPath("/", [1], [1, 2, 4, 8, /])
# LiveTree.shortestPath("/", [1, 3], [1, 2, 4, 8, /])
# EmptyTree.shortestPath: found shorter path: [1, 3, 6, /]
# LiveTree.shortestPath("/", [1, 3], [1, 3, 6, /])
1.pathTo(/): [1, 3, 6, /]    # shortest path to free space

# LiveTree.shortestPath("0")
# LiveTree.shortestPath("0", [], [])
# LiveTree.shortestPath("0", [1], [])
# LiveTree.shortestPath("0", [1, 2], [])
# LiveTree.shortestPath("0", [1, 2, 4], [])
# LiveTree.shortestPath("0", [1, 2, 4], [])
# LiveTree.shortestPath("0", [1, 2], [])
# LiveTree.shortestPath("0", [1, 2, 5], [])
# LiveTree.shortestPath("0", [1, 2, 5], [])
# LiveTree.shortestPath("0", [1], [])
# LiveTree.shortestPath("0", [1, 3], [])
# LiveTree.shortestPath("0", [1, 3], [])
1.pathTo(0): []

# LiveTree.shortestPath("1")
# LiveTree.shortestPath("1", [], [])
# LiveTree.shortestPath: found shorter path: [1]
1.pathTo(1): [1]

# LiveTree.shortestPath("10")
# LiveTree.shortestPath("10", [], [])
# LiveTree.shortestPath("10", [1], [])
# LiveTree.shortestPath("10", [1, 2], [])
# LiveTree.shortestPath("10", [1, 2, 4], [])
# LiveTree.shortestPath("10", [1, 2, 4], [])
# LiveTree.shortestPath("10", [1, 2], [])
# LiveTree.shortestPath("10", [1, 2, 5], [])
# LiveTree.shortestPath: found key. Path = [1, 2, 5, 10]
# LiveTree.shortestPath("10", [1, 2, 5], [1, 2, 5, 10])
# LiveTree.shortestPath("10", [1], [1, 2, 5, 10])
# LiveTree.shortestPath("10", [1, 3], [1, 2, 5, 10])
# LiveTree.shortestPath("10", [1, 3], [1, 2, 5, 10])
1.shortestPathTo("10"): [1, 2, 5, 10]

# LiveTree.shortestPath("6")
# LiveTree.shortestPath("6", [], [])
# LiveTree.shortestPath("6", [1], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 5], [])
# LiveTree.shortestPath("6", [1, 2, 5], [])
# LiveTree.shortestPath: found shorter path: [1, 2, 5, 6]
# LiveTree.shortestPath("6", [1], [1, 2, 5, 6])
# LiveTree.shortestPath("6", [1, 3], [1, 2, 5, 6])
# LiveTree.shortestPath("6", [1, 3], [1, 2, 5, 6])
1.pathTo(6): [1, 2, 5, 6]    # duplicate "6" selected manually

# LiveTree.shortestPath("6")
# LiveTree.shortestPath("6", [], [])
# LiveTree.shortestPath("6", [1], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2, 4], [])
# LiveTree.shortestPath("6", [1, 2], [])
# LiveTree.shortestPath("6", [1, 2, 5], [])
# LiveTree.shortestPath("6", [1, 2, 5], [])
# LiveTree.shortestPath: found key. Path = [1, 2, 5, 6]
# LiveTree.shortestPath("6", [1], [1, 2, 5, 6])
# LiveTree.shortestPath("6", [1, 3], [1, 2, 5, 6])
# LiveTree.shortestPath: found key. Path = [1, 3, 6]
# LiveTree.shortestPath("6", [1, 3], [1, 3, 6])
1.shortestPathTo("6"): [1, 3, 6]


tree = tree.insertRightOf(6, 13)
# 1.insertRightOf(6, 13)
# 2.insertRightOf(6, 13)
# 4.insertRightOf(6, 13)
# 8.insertRightOf(6, 13)
# 9.insertRightOf(6, 13)
# 5.insertRightOf(6, 13)
# 10.insertRightOf(6, 13)
# 6.insertRightOf(6, 13)
# 3.insertRightOf(6, 13)
# 6.insertRightOf(6, 13)
# 7.insertRightOf(6, 13)
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10, 6]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9, 10, 6]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10, 6]], 3)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10, 6, 13]], 2)
[[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6, 13]]
# LiveTree.shortestPath("13")
# LiveTree.shortestPath("13", [], [])
# LiveTree.shortestPath("13", [1], [])
# LiveTree.shortestPath("13", [1, 2], [])
# LiveTree.shortestPath("13", [1, 2, 4], [])
# LiveTree.shortestPath("13", [1, 2, 4], [])
# LiveTree.shortestPath("13", [1, 2], [])
# LiveTree.shortestPath("13", [1, 2, 5], [])
# LiveTree.shortestPath("13", [1, 2, 5], [])
# LiveTree.shortestPath("13", [1], [])
# LiveTree.shortestPath("13", [1, 3], [])
# LiveTree.shortestPath("13", [1, 3, 6], [])
# LiveTree.shortestPath: found key. Path = [1, 3, 6, 13]
# LiveTree.shortestPath("13", [1, 3], [1, 3, 6, 13])
tree.describePathTo("13"): [right, left, right]

tree = tree.insertLeftOf(7, 14)
# 1.insertLeftOf(7, 14)
# 2.insertLeftOf(7, 14)
# 4.insertLeftOf(7, 14)
# 8.insertLeftOf(7, 14)
# 9.insertLeftOf(7, 14)
# 5.insertLeftOf(7, 14)
# 10.insertLeftOf(7, 14)
# 6.insertLeftOf(7, 14)
# 3.insertLeftOf(7, 14)
# 6.insertLeftOf(7, 14)
# 13.insertLeftOf(7, 14)
# 7.insertLeftOf(7, 14)
# LiveTree.breadthArrays([[]], 0)
# LiveTree.breadthArrays([[1], []], 1)
# LiveTree.breadthArrays([[1], [2], []], 2)
# LiveTree.breadthArrays([[1], [2], [4], []], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8]], 3)
# LiveTree.breadthArrays([[1], [2], [4], [8, 9]], 2)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10]], 3)
# LiveTree.breadthArrays([[1], [2], [4, 5], [8, 9, 10, 6]], 1)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5], [8, 9, 10, 6]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10, 6]], 3)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6], [8, 9, 10, 6, 13]], 2)
# LiveTree.breadthArrays([[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6, 13]], 3)
[[1], [2, 3], [4, 5, 6, 7], [8, 9, 10, 6, 13, 14]]
# LiveTree.shortestPath("14")
# LiveTree.shortestPath("14", [], [])
# LiveTree.shortestPath("14", [1], [])
# LiveTree.shortestPath("14", [1, 2], [])
# LiveTree.shortestPath("14", [1, 2, 4], [])
# LiveTree.shortestPath("14", [1, 2, 4], [])
# LiveTree.shortestPath("14", [1, 2], [])
# LiveTree.shortestPath("14", [1, 2, 5], [])
# LiveTree.shortestPath("14", [1, 2, 5], [])
# LiveTree.shortestPath("14", [1], [])
# LiveTree.shortestPath("14", [1, 3], [])
# LiveTree.shortestPath("14", [1, 3, 6], [])
# LiveTree.shortestPath("14", [1, 3], [])
# LiveTree.shortestPath("14", [1, 3, 7], [])
# LiveTree.shortestPath: found key. Path = [1, 3, 7, 14]
tree.describePathTo("14"): [right, right, left]

```
