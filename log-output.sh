#!/bin/bash

rm output.md > /dev/null 2>&1
printf '# output (no trace)\n\n```shell\n' > output.md
./compile.sh src/TestTree.java && ./run.sh 1>> output.md 2>/dev/null
printf '```\n\n\n\n' >> output.md
printf '# output (with trace)\n\n```shell\n' >> output.md
./run.sh src/TestLinkedList >> output.md 2>&1
printf '```\n' >> output.md
