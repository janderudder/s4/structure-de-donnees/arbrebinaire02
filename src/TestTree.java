import Tree.Tree;


public class TestTree
{
    public static void NL() {
        System.out.println();
    }


    public static void traceNL() {
        System.err.println();
    }


    public static void main(String[] args)
    {
        Tree empty = Tree.emptyTree();

        Tree tree = Tree.makeTree("1",
            Tree.makeTree("2",
                Tree.makeTree("4"),
                Tree.makeTree("5")
            ),
            Tree.makeTree("3")
        );

        /**
         * empty tree stats
         */
        System.out.println("emptyTree.collect(): "+empty.collect());
        System.out.println("emptyTree.breadthArrays(): "+empty.breadthArrays());
        System.out.println("emptyTree.toString(): "+empty.toString());
        NL();

        System.out.println(empty+".equals(empty): "+empty.equals(empty));
        System.out.println(empty+".equals(tree): "+empty.equals(tree));
        System.out.println(empty+".countNodes(): "+empty.countNodes());
        System.out.println(empty+".countLeaves(): "+empty.countLeaves());
        System.out.println(empty+".isEmpty(): "+empty.isEmpty());
        NL();

        /**
         * tree stats
         */
        System.out.println("tree.collect(): "+tree.collect());
        System.out.println("tree.breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.toString(): "+tree.toString());
        NL();

        System.out.println(tree+".equals(empty): "+tree.equals(empty));
        System.out.println(tree+".equals(tree): "+tree.equals(tree));
        System.out.println(tree+".countNodes(): "+tree.countNodes());
        System.out.println(tree+".countLeaves(): "+tree.countLeaves());
        System.out.println(tree+".isEmpty(): "+tree.isEmpty());
        NL();

        /**
         * exploration
         */
        System.out.println(tree+".left(): "+tree.left());
        System.out.println(tree+".right(): "+tree.right());
        System.out.println(tree+".leftMost(): "+tree.leftMost());
        System.out.println(tree+".rightMost(): "+tree.rightMost());
        System.out.println(tree+".left().right(): "+tree.left().right());
        System.out.println(tree+".right().left(): "+tree.right().left());
        NL();

        /**
         * copy, collect and mirror
         */
        System.out.println(empty+".copy().breadthArrays(): "+empty.copy().breadthArrays());
        System.out.println(empty+".mirror().breadthArrays(): "+empty.mirror().breadthArrays());
        System.out.println(tree+".copy().breadthArrays(): "+tree.copy().breadthArrays());
        System.out.println(tree+".mirror().breadthArrays: "+tree.mirror().breadthArrays());
        System.out.println(tree+".copy().collect(): "+tree.copy().collect());
        System.out.println(tree+".mirror().collect(): "+tree.mirror().collect());
        NL();

        /**
         * search
         */
        System.out.println(empty+".contains("+empty+"): "+empty.contains(empty));
        System.out.println(tree+".contains("+empty+"): "+tree.contains(empty));
        System.out.println(tree+".contains(\"0\"): "+tree.contains("0"));
        System.out.println(tree+".contains("+tree+"): "+tree.contains(tree));
        System.out.println(tree+".contains(\"2\"): "+tree.contains("2"));
        System.out.println(tree+".contains("+tree.right()+"): "+tree.contains(tree.right()));
        NL();

        System.out.println(empty+".searchLeftRight(\""+empty.toString()+"\"): "+empty.searchLeftRight(empty.toString()));
        System.out.println(empty+".searchLeftRight(\"0\"): "+empty.searchLeftRight("0"));
        System.out.println(empty+".searchLeftRight(\""+tree.value()+"\"): "+empty.searchLeftRight(tree.value()));
        System.out.println(tree+".searchLeftRight(\""+empty.toString()+"\"): "+empty.searchLeftRight(empty.toString()));
        System.out.println(tree+".searchLeftRight(\"0\"): "+tree.searchLeftRight("0"));
        System.out.println(tree+".searchLeftRight(\""+tree.value()+"\"): "+tree.searchLeftRight(tree.value()));
        System.out.println(tree+".searchLeftRight(\"2\"): "+tree.searchLeftRight("2"));
        System.out.println(tree+".searchLeftRight(\""+tree.right()+"\"): "+tree.searchLeftRight(tree.right().value()));
        NL();

        /**
         * insertion
         */
        System.out.println("tree = tree.insertRight(\"7\")");
        tree = tree.insertRight("7");
        System.out.println(tree+".breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.describePathTo(\"7\"): "+tree.describePathTo("7"));
        NL();

        System.out.println("tree = tree.insertLeft(\"8\")");
        tree = tree.insertLeft("8");
        System.out.println(tree+".breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.describePathTo(\"8\"): "+tree.describePathTo("8"));
        NL();

        System.out.println("tree = tree.insert(\"9\")    # tries left, then right, then goes left");
        tree = tree.insert("9");
        System.out.println(tree+".breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.describePathTo(\"9\"): "+tree.describePathTo("9"));
        NL();

        System.out.println("tree = tree.insertBreadthFirst(\"6\")");
        tree = tree.insertBreadthFirst("6");
        System.out.println(tree+".breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.describePathTo(\"6\"): "+tree.describePathTo("6"));
        NL();

        System.out.println("tree = tree.insertBreadthFirst(\"10\")");
        tree = tree.insertBreadthFirst("10");
        System.out.println(tree+".breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.describePathTo(\"10\"): "+tree.describePathTo("10"));
        NL();

        System.out.println("tree = tree.insertBreadthFirst(\"6\")    # duplicate on purpose");
        tree = tree.insertBreadthFirst("6");
        System.out.println(tree+".breadthArrays(): "+tree.breadthArrays());
        System.out.println("tree.describePathTo(\"6\"): "+tree.describePathTo("6")+"    # gives shortest path");
        NL();

        /**
         * search path
         */
        System.out.println(empty+".pathTo("+empty+"): "+empty.pathTo(empty)); traceNL();
        System.out.println(empty+".pathTo("+tree+"): "+empty.pathTo(tree)); traceNL();
        System.out.println(tree+".pathTo("+empty+"): "+tree.pathTo(empty)+"    # shortest path to free space"); traceNL();
        System.out.println(tree+".pathTo("+Tree.makeTree("0")+"): "+tree.pathTo(Tree.makeTree("0"))); traceNL();
        System.out.println(tree+".pathTo("+tree+"): "+tree.pathTo(tree)); traceNL();
        System.out.println(tree+".shortestPathTo(\"10\"): "+tree.shortestPathTo("10")); traceNL();
        System.out.println(tree+".pathTo("+tree.left().right().right()+"): "+tree.pathTo(tree.left().right().right())+"    # duplicate \"6\" selected manually"); traceNL();
        System.out.println(tree+".shortestPathTo(\"6\"): "+tree.shortestPathTo("6")); traceNL();
        NL();

        /**
         * special insert
         */
        System.out.println("tree = tree.insertRightOf("+tree.right().left()+", 13)");
        tree = tree.insertRightOf(tree.right().left(), "13");
        System.out.println(tree.breadthArrays());
        System.out.println("tree.describePathTo(\"13\"): "+tree.describePathTo("13"));
        NL();

        System.out.println("tree = tree.insertLeftOf("+tree.right().right()+", 14)");
        tree = tree.insertLeftOf(tree.right().right(), "14");
        System.out.println(tree.breadthArrays());
        System.out.println("tree.describePathTo(\"14\"): "+tree.describePathTo("14"));
        NL();
    }
}
