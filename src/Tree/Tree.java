package Tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;


public abstract class Tree
{
    /**
     * Factory
     */
    public static Tree emptyTree()
    {
        return EmptyTree.node;
    }

    public static Tree makeTree(String value)
    {
        return new LiveTree(value);
    }

    public static Tree makeTree(String value, Tree leftNode)
    {
        return new LiveTree(value, leftNode);
    }

    public static Tree makeTree(String value, Tree leftNode, Tree rightNode)
    {
        return new LiveTree(value, leftNode, rightNode);
    }

    /**
     * static interface
     */
    static protected String emptyTreeString = "/";

    static public void setEmptyTreeString(String representation) {
        Tree.emptyTreeString = representation;
    }

    /**
     * abstract interface
     */
    public abstract String value();

    public abstract Tree left();
    public abstract Tree right();
    public abstract Tree leftMost();
    public abstract Tree rightMost();

    public abstract boolean isEmpty();
    public abstract int countNodes();
    public abstract int countLeaves();

    public abstract boolean contains(Tree node);
    public abstract boolean contains(String value);
    public abstract Tree searchLeftRight(String value);
    public abstract ArrayList<Tree> pathTo(Tree node);
    public abstract ArrayList<Tree> shortestPathTo(String value);
    public abstract ArrayList<String> describePathTo(Tree node);
    public abstract ArrayList<String> describePathTo(String value);

    public abstract Tree copy();
    public abstract Tree mirror();

    public abstract ArrayList<String> collect();
    public abstract ArrayList<ArrayList<Tree>> breadthArrays();
    public abstract ArrayList<ArrayList<String>> breadthValuesArrays();

    public abstract Tree insert(String value);
    public abstract Tree insertBreadthFirst(String value);
    public abstract Tree insertLeft(String value);
    public abstract Tree insertRight(String value);
    public abstract Tree insertLeftOf(Tree node, String value);
    public abstract Tree insertRightOf(Tree node, String value);

    public abstract void displayLNR();

    /**
     * Object
     */
    @Override
    public String toString() {
        return this.value();
    }

    /**
     * private
     */
    protected abstract Tree searchLeftRight(String key, ArrayList<Tree> unexploredNodes); // tail call optimization
    protected abstract ArrayList<String> collect(ArrayList<String> values);
    protected abstract ArrayList<ArrayList<Tree>> breadthArrays(ArrayList<ArrayList<Tree>> floors, int rank);
    protected abstract ArrayList<ArrayList<String>> breadthValuesArrays(ArrayList<ArrayList<String>> floors, int rank);
    protected abstract void pathTo(Tree node, ArrayList<Tree> currentPath, ArrayList<Tree> path);
    protected abstract void shortestPathTo(String key, ArrayList<Tree> path, ArrayList<Tree> shortestPath);

    /**
     * trace
     */
    public static void traceDebug(String... strings) {
        System.out.print("# ");
        for (String string : strings) {
            System.out.print(string);
        }
        System.out.println();
    }

    protected static void trace(String ...strings) {
        System.err.print("# ");
        for (String string : strings) {
            System.err.print(string);
        }
        System.err.println();
    }

    protected static String join(ArrayList<Tree> nodeList) {
        return Arrays.stream(nodeList.toArray())
            .map(node -> node.toString())
            .collect(Collectors.joining(", "));
    }

}




class EmptyTree extends Tree
{
    static <T> T __unreachableMethod__(T returnValue) {
        assert(false);
        return returnValue;
    }



    /**
     * singleton instance
     */
    protected static EmptyTree node = new EmptyTree();

    private EmptyTree() {}



    @Override
    public String value() {
        return null;
    }



    @Override
    public Tree left() {
        return null;
    }

    @Override
    public Tree right() {
        return null;
    }

    @Override
    public Tree leftMost() {
        return null;
    }

    @Override
    public Tree rightMost() {
        return null;
    }



    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public int countNodes() {
        return 0;
    }

    @Override
    public int countLeaves() {
        return 0;
    }



    @Override
    public boolean contains(Tree node) {
        return false;
    }

    @Override
    public boolean contains(String key) {
        return false;
    }

    @Override
    public Tree searchLeftRight(String key) {
        return this;
    }

    @Override
    protected Tree searchLeftRight(String key, ArrayList<Tree> unexploredNodes)
    {
        trace("EmptyTree.searchLeftRight(\"", key, "\", [ ",
            join(unexploredNodes), " ])");

        if (!unexploredNodes.isEmpty()) {
            Tree node = unexploredNodes.remove(0);
            return node.searchLeftRight(key, unexploredNodes);
        } else {
            return this;
        }
    }


    @Override
    public ArrayList<Tree> pathTo(Tree node)
    {
        if (node==null || node.isEmpty()) {
            ArrayList<Tree> path = new ArrayList<>();
            path.add(this);
            return path;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    protected void pathTo(
        Tree node,
        ArrayList<Tree> path,
        ArrayList<Tree> shortestPath
    ){
        path.add(this);
        if (node.isEmpty()
            && (shortestPath.isEmpty() || path.size() < shortestPath.size()))
        {
            trace("EmptyTree.shortestPath: found shorter path: "+path);
            shortestPath.clear();
            shortestPath.addAll(path);
        }
    }


    @Override
    public ArrayList<Tree> shortestPathTo(String key) {
        return new ArrayList<>();
    }

    @Override
    protected void shortestPathTo(
        String key,
        ArrayList<Tree> path,
        ArrayList<Tree> shortestPath
    ) {}


    @Override
    public ArrayList<String> describePathTo(Tree node)
    {
        if (node==null || node.isEmpty()) {
            ArrayList<String> path = new ArrayList<>();
            path.add("self");
            return path;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public ArrayList<String> describePathTo(String value)
    {
        // if (node==null || node.isEmpty()) {
        //     ArrayList<String> path = new ArrayList<>();
        //     path.add("self");
        //     return path;
        // } else {
        //     return new ArrayList<>();
        // }
        return new ArrayList<>();
    }



    @Override
    public Tree copy() {
        return this;
    }

    @Override
    public Tree mirror() {
        return this;
    }



    @Override
    public ArrayList<String> collect() {
        return new ArrayList<String>();
    }

    @Override
    protected ArrayList<String> collect(ArrayList<String> values) {
        return values;
    }

    @Override
    public ArrayList<ArrayList<Tree>> breadthArrays() {
        return new ArrayList<>();
    }

    @Override
    protected ArrayList<ArrayList<Tree>> breadthArrays(
        ArrayList<ArrayList<Tree>> floors, int rank)
    {
        return floors;
    }

    @Override
    public ArrayList<ArrayList<String>> breadthValuesArrays() {
        return new ArrayList<>();
    }

    @Override
    protected ArrayList<ArrayList<String>> breadthValuesArrays(
        ArrayList<ArrayList<String>> floors, int rank)
    {
        return floors;
    }



    @Override
    public Tree insert(String value) {
        return Tree.makeTree(value);
    }

    @Override
    public Tree insertBreadthFirst(String value) {
        return this.insert(value);
    }

    @Override
    public Tree insertLeft(String value) {
        return this.insert(value);
    }

    @Override
    public Tree insertRight(String value) {
        return this.insert(value);
    }

    @Override
    public Tree insertLeftOf(Tree node, String value)
    {
        if (node == null || node.isEmpty()) {
            return this.insertLeft(value);
        } else {
            return this;
        }
    }

    @Override
    public Tree insertRightOf(Tree node, String value)
    {
        if (node == null || node.isEmpty()) {
            return this.insertRight(value);
        } else {
            return this;
        }
    }


    @Override
    public void displayLNR() {}


    /**
     * Object
     */
    @Override
    public String toString() {
        return Tree.emptyTreeString;
    }

    @Override
    public boolean equals(Object object)
    {
        if (object instanceof EmptyTree) {
            return false;
        }
        else {
            return super.equals(object);
        }
    }
}



class LiveTree extends Tree
{
    String _value;
    Tree   _left;
    Tree   _right;

    LiveTree(String value)
    {
        this._value = value;
        this._left = EmptyTree.node;
        this._right = EmptyTree.node;
    }

    LiveTree(final String value, final Tree left)
    {
        if (left == null) {
            this._left = EmptyTree.node;
        } else {
            this._left = left;
        }

        this._left = (left!=null) ? left : EmptyTree.node;

        this._value = value;
        this._right = EmptyTree.node;
    }

    LiveTree(String value, Tree left, Tree right)
    {
        if (left == null) {
            this._left = EmptyTree.node;
        } else {
            this._left = left;
        }
        if (right == null) {
            this._right = EmptyTree.node;
        } else {
            this._right = right;
        }
        this._value = value;
    }

    @Override
    public String value()
    {
        return this._value;
    }



    @Override
    public Tree left()
    {
        return this._left;
    }

    @Override
    public Tree right()
    {
        return this._right;
    }

    @Override
    public Tree leftMost()
    {
        return this._left.isEmpty() ? this : this._left.leftMost();
    }

    @Override
    public Tree rightMost()
    {
        return this._right.isEmpty() ? this : this._right.rightMost();
    }



    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public int countNodes() {
        return 1 + this._left.countNodes() + this._right.countNodes();
    }

    @Override
    public int countLeaves()
    {
        return (this._left.isEmpty() && this._right.isEmpty())
            ? 1
            : this.left().countLeaves() + this.right().countLeaves();
    }



    @Override
    public boolean contains(Tree node)
    {
        return node==null || node.isEmpty() || this == node
            || this._left.contains(node)
            || this._right.contains(node);
    }

    @Override
    public boolean contains(String key)
    {
        return this._value.equals(key)
            || this._left.contains(key)
            || this._right.contains(key);
    }

    @Override
    public Tree searchLeftRight(String key)
    {
        System.err.println(); trace("LiveTree.searchLeftRight(\"", key, "\")");

        return this.searchLeftRight(key, new ArrayList<>());
    }

    @Override
    protected Tree searchLeftRight(String key, ArrayList<Tree> unexploredNodes)
    {
        trace("EmptyTree.searchLeftRight(\"", key, "\", [ ", join(unexploredNodes), " ])");

        if (this._value.equals(key)) {
            return this;
        }
        unexploredNodes.add(this._right);
        return this._left.searchLeftRight(key, unexploredNodes);
    }


    @Override
    public ArrayList<Tree> pathTo(Tree node)
    {
        trace("LiveTree.shortestPath(\""+node+"\")");

        ArrayList<Tree> shortestPath =  new ArrayList<>();
        Tree keyNode = (node != null) ? node : Tree.emptyTree();

        this.pathTo(keyNode, new ArrayList<>(), shortestPath);
        return shortestPath;
    }

    @Override
    protected void pathTo(
        Tree node,
        ArrayList<Tree> currentPath,
        ArrayList<Tree> path
    )
    {
        trace("LiveTree.shortestPath(\""+node+"\", "+currentPath+", "+path+")");
        currentPath.add(this);

        if (this == node)
        {
            trace("LiveTree.shortestPath: found shorter path: "+currentPath);
            path.clear();
            path.addAll(currentPath);
        }
        else {
            ArrayList<Tree> rightPath = new ArrayList<>(currentPath);
            this._left.pathTo(node, currentPath, path);
            this._right.pathTo(node, rightPath, path);
        }
    }


    @Override
    public ArrayList<Tree> shortestPathTo(String key)
    {
        trace("LiveTree.shortestPath(\""+key+"\")");
        ArrayList<Tree> shortestPath =  new ArrayList<>();
        this.shortestPathTo(key, new ArrayList<>(), shortestPath);
        return shortestPath;
    }

    @Override
    protected void shortestPathTo(
        String key,
        ArrayList<Tree> path,
        ArrayList<Tree> shortestPath
    ){
        trace("LiveTree.shortestPath(\""+key+"\", "+path+", "+shortestPath+")");
        path.add(this);

        if (this._value.equals(key)
            && (shortestPath.isEmpty() || path.size() < shortestPath.size()))
        {
            trace("LiveTree.shortestPath: found key. Path = "+path);
            shortestPath.clear();
            shortestPath.addAll(path);
        }
        else {
            ArrayList<Tree> rightPath = new ArrayList<>(path);
            this._left.shortestPathTo(key, path, shortestPath);
            this._right.shortestPathTo(key, rightPath, shortestPath);
        }
    }

    @Override
    public ArrayList<String> describePathTo(Tree node)
    {
        ArrayList<Tree> nodesPath = this.pathTo(node);

        if (nodesPath.isEmpty()) {
            return new ArrayList<>();
        }

        ArrayList<String> strPath = new ArrayList<>();

        if (nodesPath.size() == 1) {
            strPath.add("self");
        }
        else {
            for (int i=1; i < nodesPath.size(); ++i)
            {
                Tree parent = nodesPath.get(i-1);
                Tree currentNode = nodesPath.get(i);
                if (currentNode == this) {
                    strPath.add("self");
                }
                else if (currentNode == parent.left()) {
                    strPath.add("left");
                } else {
                    strPath.add("right");
                }
            }
        }
        return strPath;
    }

    @Override
    public ArrayList<String> describePathTo(String value)
    {
        ArrayList<Tree> nodesPath = this.shortestPathTo(value);

        if (nodesPath.isEmpty()) {
            return new ArrayList<>();
        }

        ArrayList<String> strPath = new ArrayList<>();

        if (nodesPath.size() == 1) {
            strPath.add("self");
        }
        else {
            for (int i=1; i < nodesPath.size(); ++i)
            {
                Tree parent = nodesPath.get(i-1);
                Tree currentNode = nodesPath.get(i);
                if (currentNode == this) {
                    strPath.add("self");
                }
                else if (currentNode == parent.left()) {
                    strPath.add("left");
                } else {
                    strPath.add("right");
                }
            }
        }
        return strPath;
    }



    @Override
    public Tree copy()
    {
        return Tree.makeTree(this._value, this._left.copy(), this._right.copy());
    }

    @Override
    public Tree mirror()
    {
        return
            Tree.makeTree(this._value, this._right.mirror(), this._left.mirror());
    }



    @Override
    public ArrayList<String> collect()
    {
        return this.collect(new ArrayList<>());
    }

    @Override
    protected ArrayList<String> collect(ArrayList<String> values)
    {
        values.add(this._value);
        this._left.collect(values);
        this._right.collect(values);
        return values;
    }


    @Override
    public ArrayList<ArrayList<Tree>> breadthArrays()
    {
        ArrayList<ArrayList<Tree>> floors = new ArrayList<>();
        floors.add(new ArrayList<>());
        return this.breadthArrays(floors, 0);
    }

    @Override
    protected ArrayList<ArrayList<Tree>> breadthArrays(
        ArrayList<ArrayList<Tree>> floors,
        int rank
    ){
        trace("LiveTree.breadthArrays("+floors+", "+rank+")");

        ArrayList<Tree> floor = floors.get(rank);
        floor.add(this);

        if (floors.size() == rank+1 && (!this._left.isEmpty() || !this._right.isEmpty())) {
            floors.add(new ArrayList<>());
        }

        this._left.breadthArrays(floors, rank+1);
        this._right.breadthArrays(floors, rank+1);

        return floors;
    }

    @Override
    public ArrayList<ArrayList<String>> breadthValuesArrays()
    {
        ArrayList<ArrayList<String>> floors = new ArrayList<>();
        floors.add(new ArrayList<>());
        return this.breadthValuesArrays(floors, 0);
    }

    @Override
    protected ArrayList<ArrayList<String>> breadthValuesArrays(
        ArrayList<ArrayList<String>> floors,
        int rank
    ){
        trace("LiveTree.breadthValuesArrays("+floors+", "+rank+")");

        ArrayList<String> floor = floors.get(rank);
        floor.add(this._value);

        if (floors.size() == rank+1 && (!this._left.isEmpty() || !this._right.isEmpty())) {
            floors.add(new ArrayList<>());
        }

        this._left.breadthValuesArrays(floors, rank+1);
        this._right.breadthValuesArrays(floors, rank+1);

        return floors;
    }



    @Override
    public Tree insertBreadthFirst(String value)
    {
        ArrayList<ArrayList<Tree>> floors = this.copy().breadthArrays();

        boolean inserted = false;
        int i = 0, j = 0;
        while (!inserted)
        {
            ArrayList<Tree> floor = floors.get(i);
            LiveTree node = (LiveTree)floor.get(j);

            if (++j == floor.size()) {
                j = 0;
                ++i;
            }

            if (node._left.isEmpty()) {
                node._left = Tree.makeTree(value);
                inserted = true;
            }
            else if (node._right.isEmpty()) {
                node._right = Tree.makeTree(value);
                inserted = true;
            }
        }
        return floors.get(0).get(0);
    }

    @Override
    public Tree insert(String value)
    {
        if (this._left.isEmpty())
        {
            return Tree.makeTree(
                this._value,
                Tree.makeTree(value),
                this._right.copy()
            );
        }
        else if (this._right.isEmpty())
        {
            return Tree.makeTree(
                this._value,
                this._left.copy(),
                Tree.makeTree(value)
            );
        }
        else
        {
            return Tree.makeTree(
                this._value,
                this._left.insert(value),
                this._right.copy()
            );
        }
    }

    @Override
    public Tree insertLeft(String value)
    {
        return Tree.makeTree(
            this._value,
            this._left.insertLeft(value),
            this._right.copy()
        );
    }

    @Override
    public Tree insertRight(String value)
    {
        return Tree.makeTree(
            this._value,
            this._left.copy(),
            this._right.insertRight(value)
        );
    }

    @Override
    public Tree insertLeftOf(Tree node, String value)
    {
        trace(this+".insertLeftOf("+node,", "+value+")");

        if (node == null) {
            node = Tree.emptyTree();
        }

        if (node == this) {
            return this.insertLeft(value);
        }
        else
        {
            return Tree.makeTree(
                this._value,
                this._left.insertLeftOf(node, value),
                this._right.insertLeftOf(node, value)
            );
        }
    }

    @Override
    public Tree insertRightOf(Tree node, String value)
    {
        trace(this+".insertRightOf("+node,", "+value+")");

        if (node == null) {
            node = Tree.emptyTree();
        }

        if (node == this) {
            return this.insertRight(value);
        }
        else
        {
            return Tree.makeTree(
                this._value,
                this._left.insertRightOf(node, value),
                this._right.insertRightOf(node, value)
            );
        }
    }



    static int counter = 0;

    @Override
    public void displayLNR()
    {
        this._left.displayLNR();
        System.out.println(this);
        this._right.displayLNR();
    }


    /**
     * Object
     */
     @Override
    public boolean equals(Object object)
    {
        if (object instanceof EmptyTree) {
            return false;
        }
        else {
            return super.equals(object);
        }
    }
}
