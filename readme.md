# TP Tree (Immutable)

### Compile and run

    ./compile.sh && ./run.sh

### Don't output trace

    ./run.sh 2> /dev/null

### Output

See [output](output.md)
